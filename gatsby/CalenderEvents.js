const crypto = require('crypto');

const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/calendar.readonly'];

// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);

    console.log('[+] Getting Token');
    // Check if we have previously stored a token.
    let token;
    try {
      token = fs.readFileSync(TOKEN_PATH);
      console.log('[+] Token Found in file', TOKEN_PATH);

      oAuth2Client.setCredentials(JSON.parse(token.toString()));
      callback && callback(oAuth2Client);
    } catch (error) {
      console.log('[-] File Not Found', error);
    }

    if (!token) {
      console.log('[-] Token Not Found in file', TOKEN_PATH);
      // It shouldn't happen
      getAccessToken(oAuth2Client, callback);
    }
    return oAuth2Client
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getAccessToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error('Error retrieving access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) return console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}

/**
 * Lists the next 10 events on the user's primary calendar.
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
async function listEvents(auth) {
    const calendar = google.calendar({ version: 'v3', auth });
    console.log('[+] listEvents');
    const res = await calendar.events.list({
        calendarId: 'primary',
        timeMin: (new Date()).toISOString(),
        maxResults: 10,
        singleEvents: true,
        orderBy: 'startTime',
    });

    const events = res.data.items;
    if (events.length) {
        console.log('Upcoming 10 events:');
        events.map((event, i) => {
            const start = event.start.dateTime || event.start.date;
            console.log(`${start} - ${event.summary}`);
        });
        return events;
    } else {
        console.log('No upcoming events found.');
        return [];
    }
}

function parseDescription(description) {
  const descriptionData = {}
  if(!description) {
    return descriptionData;
  }
  const lines = description.split('\n');
  lines.map(lineItem => {
    if(lineItem) {
      // const [key, value] = lineItem.split(':');
      const [key, value] = lineItem.replace(/:/,'$').split('$');
      // Check if there is no blank value, otherwise don't added to descriptionData
      if(value && value.trim()) {
        descriptionData[key] = value.trim();
      }
    }
  })
  console.log('descriptionData', descriptionData);
  return descriptionData;
}

exports.parseDescription = parseDescription;
exports.listEvents = listEvents;
exports.authorize = authorize;