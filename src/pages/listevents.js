import React from 'react'
import { Button } from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.css';

function listevents() {
    return (
        <div>
            <Button color="primary">primary</Button>
            <Button color="danger">Danger!</Button>
        </div>
    )
}

export default listevents
