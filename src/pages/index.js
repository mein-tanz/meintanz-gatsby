import React from "react"
import { graphql } from 'gatsby'
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';

// import { Link } from "gatsby"

import Layout from "../components/layout"
// import Image from "../components/image"
// import SEO from "../components/seo"

const IndexPage = (props) => {
  const events = props.data.allCalEvents.nodes;

  return (
    <Layout>

      {/* <SEO title="Home" /> */}

      {/* events : { JSON.stringify(events) } */}

      { 
        events.map(event => {
          return (
          <div>
      <Card>
        {/* <CardImg top width="100%" src="/assets/318x180.svg" alt="Card image cap" /> */}
        <CardBody>
                  <CardTitle>{event.summary}</CardTitle>
                  <CardSubtitle>{event.start.dateTime} {event.starttime}, {event.location}</CardSubtitle>
          <CardText>
                    <ul>
                      {/* Show only if not falsy */}
                      {event.descriptionData.Leitung ?
                        <li>{event.descriptionData.Leitung}</li>
                        : null}

                      {event.descriptionData.DJ ?
                        <li>{event.descriptionData.DJ}</li>
                        : null}

                      {event.descriptionData.Kosten ?
                        <li>{event.descriptionData.Kosten}</li>
                        : null}

                      <li><a href="{{event.descriptionData.Quelle}}"></a>{event.descriptionData.Quelle}</li>

                      {event.descriptionData.Einlass ?
                        <li>{event.descriptionData.Einlass}</li>
                        : null}

                      {event.descriptionData.Anmeldung ?
                        <li>{event.descriptionData.Anmeldung}</li>
                        : null}


                      {event.descriptionData.Link && event.descriptionData.URL ?
                        <div>
                          <li>{event.descriptionData.Link}</li>
                          <li>{event.descriptionData.URL}</li>
                        </div>
                        : null}

                      <li>Event Ende: {event.endtime}</li>
                    </ul>
          </CardText>
          {/* <Button>Button</Button> */}
        </CardBody>
      </Card>
            
            </div>)
        }) 
      }

      {/* <p>Welcome to your new Gatsby site.</p>
      <p>Now go build something great.</p>
      <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
        <Image />
      </div>
      <Link to="/page-2/">Go to page 2</Link> <br />
      <Link to="/using-typescript/">Go to "Using TypeScript"</Link> */}
    </Layout>
  )
}

export default IndexPage

export const query = graphql`
  query CalEventsQuery {
    allCalEvents {
      nodes {
        summary
        location
        starttime
        endtime
        descriptionData {
          Typ
          Anmeldung
          DJ
          Einlass
          Kosten
          Leitung
          Link
          Quelle
          URL
        }
        start {
          dateTime(formatString: "dd DD.MM.", locale: "de_CH")
        }
        end {
          dateTime(formatString: "dd, DD.MM.yyyy", locale: "de_CH")
        }
      }
    }
  }
`;