const fs = require('fs');
const crypto = require('crypto');

const { authorize, listEvents, parseDescription } = require('./gatsby/CalenderEvents');

exports.sourceNodes = async ({ actions }) => {
  const { createNode } = actions;

  const credentials = fs.readFileSync('credentials.json');

  // Authorize a client with credentials, then call the Google Calendar API.
  console.log('[+] Authorizing');
  const auth = authorize(JSON.parse(credentials.toString()));

  // Get data
  console.log('[+] Getting Events');
  const events = await listEvents(auth);

  // Add Start and Endtime
  
  // map into these results and create nodes
  events.map((event, i) => {
    
    events[i].starttime = events[i].start.dateTime.substr(11, 5);
    events[i].endtime = events[i].end.dateTime.substr(11, 5);

    const descriptionData =  { 
      Typ: '',
      Leitung: '',
      DJ: '',
      Einlass: '',
      Quelle: '',
      Kosten: '',
      Anmeldung: '',
      Link: '',
      URL: '' 
    };

    // Create your node object
    const eventNode = {
      // Required fields
      id: `${i}`,
      parent: `__SOURCE__`,
      internal: {
        type: `CalEvents`,
      },
      children: [],

      // fields to query
      ...event,
      descriptionData: { ...descriptionData,  ...parseDescription(event.description) }
      // id: event.id,
      // summary: event.summary,
      // status: event.status,
      // created: event.created,
      // htmlLink: event.htmlLink,
      // start: event.start,
      // end: event.end,
    }

    // Get content digest of node. (Required field)
    const contentDigest = crypto
      .createHash(`md5`)
      .update(JSON.stringify(eventNode))
      .digest(`hex`);

      eventNode.internal.contentDigest = contentDigest;

    // Create node with the gatsby createNode() API
    createNode(eventNode);
  });

  return;
}

if(require.main == module) {
  console.log('[+] Generating Token')

  const credentials = fs.readFileSync('credentials.json');
  const creds = JSON.parse(credentials.toString());

  // Authorize a client with credentials, then call the Google Calendar API.
  console.log('[+] Authorizing');
  authorize(creds, (auth) => {
    console.log('[+] Getting Events');
    
    listEvents(auth).then(events => {
      console.log('[+] Events :', events);
    });
  });


}
